package com.bestbizsvc.zzzpal;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.util.Pair;

import java.text.SimpleDateFormat;


public class AlarmService
extends IntentService
{
	public static final String ACTION_ALARM = "ALARM_ACTION"
	,   ACTION_TEST ="ACTION_TEST"
	,   STATUS_TEST="STATUS_TEST"
	,   ACTION_MAIN ="MAIN_PATTERN"
	,   STATUS_SEQUENTIAL_ALARMS ="SEQ_ALMS"
	,   BUNDLE_CUR_STATUS = "curr_status"
	;
	private String m_currentState
	;
	private int m_nPulseDuration
	,   m_nPulse_number
	,   m_nMain_cycle_minutes
	,   m_nAlarm_cycle_minutes
	;
	private boolean m_bUselight_switch
	,   m_bUsesound_switch
	,   m_bUsevib_switch
	,   m_bHasLight
	,   m_bCameraOn
	;
	Camera m_mycam;

	public AlarmService (String name) {

		super(name);
	}

	public AlarmService() {
		super(AlarmService.class.getName());
	}

	private void toggleCamera() {
		m_bCameraOn= ! m_bCameraOn;
		CameraManager cameraManager = (CameraManager) getApplicationContext().getSystemService(Context.CAMERA_SERVICE);
		try {
			if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
				for (String id : cameraManager.getCameraIdList()) {
					// Turn on the flash if camera has one
					if(cameraManager.getCameraCharacteristics(id).get(CameraCharacteristics.FLASH_INFO_AVAILABLE)) {
						if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
							cameraManager.setTorchMode(id, m_bCameraOn);
						}
					}
				}
			} else if( this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH) ) {
				if(m_bCameraOn) {
					m_mycam = Camera.open();
					Camera.Parameters parms = m_mycam.getParameters();
					parms.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
					m_mycam.setParameters(parms);
				} else {
					Camera.Parameters parms = m_mycam.getParameters();
					parms.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
					m_mycam.setParameters(parms);
					m_mycam.release();
					m_mycam=null;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void DoBuzz() {
		m_bCameraOn=false;
		for (int nIter = 0; nIter < m_nPulse_number; nIter++) {
			if(m_bHasLight && m_bUselight_switch)
				toggleCamera();

			if(m_bUsesound_switch) {
				ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
				toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, m_nPulseDuration);
			}
			if(m_bUsevib_switch) {
				Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
				v.vibrate(m_nPulseDuration);
			}

			if(1 < m_nPulse_number || m_bCameraOn) {
				try {
					Thread.sleep(2 * m_nPulseDuration);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			if(m_bCameraOn)
				toggleCamera();
		}
	}

	public Pair<Long, String> calcMsgString(int nMinutes)
	{
		SimpleDateFormat dateformat = new SimpleDateFormat("hh:mm:ss");
		long nowMillis=System.currentTimeMillis()
				,   alarmMillis=nowMillis+(1000*60*nMinutes)
				;
		String nextAlarmTime = dateformat.format(alarmMillis)
				,   msgString="next alarm in "+nMinutes+" minutes on "+nextAlarmTime
				;
		return new Pair(alarmMillis,msgString);
	}

	@Override
	protected void onHandleIntent
	(
			@Nullable Intent intent
	) {
		getPreferences();
		Messenger messenger=null;
		Message msg=null;

		if(intent != null) {
			Intent alarmIntent = new Intent(this, AlarmService.class).setAction(ACTION_ALARM);

			Bundle bundle = intent.getExtras();
			if (bundle != null) {
				messenger = (Messenger) bundle.get("messenger");
				msg = Message.obtain();
			}

			String action = intent.getAction()
			;
			if(action != null) {
				switch(action) {
					case ACTION_ALARM:
						DoBuzz();
						m_currentState=(String)bundle.getCharSequence(BUNDLE_CUR_STATUS);
						if(STATUS_SEQUENTIAL_ALARMS.equals(m_currentState)) {
							Pair<Long, String> pData=calcMsgString(m_nAlarm_cycle_minutes);
							long alarmMillis=pData.first
									;
							String msgString=pData.second
									;
							if(null!=messenger) {
								try {
									Bundle myBundle = new Bundle();
									myBundle.putSerializable("msg",msgString);
									msg.setData(myBundle);
									messenger.send(msg); // update GUI
									alarmIntent.putExtra("messenger", messenger);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
							// set the alarm
							alarmIntent.putExtra(BUNDLE_CUR_STATUS, STATUS_SEQUENTIAL_ALARMS);
							setAlarm(alarmMillis, alarmIntent);
						}
						break;
					case ACTION_TEST:
						DoBuzz();
						if(null!=messenger) {
							try {
								Bundle myBundle = new Bundle();
								myBundle.putSerializable("msg","Test done");
								msg.setData(myBundle);
								messenger.send(msg);
								alarmIntent.putExtra("messenger", messenger);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						break;
					case ACTION_MAIN:
						Pair<Long, String> pData=calcMsgString(m_nMain_cycle_minutes);
						long alarmMillis=pData.first
						;
						String msgString=pData.second
						;
						if(null!=messenger) {
							try {
								Bundle myBundle = new Bundle();
								myBundle.putSerializable("msg",msgString);
								msg.setData(myBundle);
								messenger.send(msg); // update GUI
								alarmIntent.putExtra("messenger", messenger);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
// set the alarm
						alarmIntent.putExtra(BUNDLE_CUR_STATUS, STATUS_SEQUENTIAL_ALARMS);
						setAlarm(alarmMillis, alarmIntent);
						break;
				}
			}
		}
	}

	private void getPreferences () {
			try{
				SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
				m_nPulseDuration = Integer.valueOf(settings.getString("pulse_duration", "1"));
				m_nPulse_number = Integer.valueOf(settings.getString("pulse_number", "1"));
				m_bUselight_switch = settings.getBoolean("uselight_switch", true);
				m_bUsesound_switch = settings.getBoolean("usesound_switch", true);
				m_bUsevib_switch = settings.getBoolean("usevib_switch", true);
				m_nMain_cycle_minutes = Integer.valueOf(settings.getString("main_cycle_minutes", "3600"));
				m_nAlarm_cycle_minutes = Integer.valueOf(settings.getString("alarm_cycle_minutes", "20"));
				m_bHasLight=getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
			} catch (Exception e) {
				e.printStackTrace();
			}
	}


	void setAlarm(long time, Intent intent) {
		PendingIntent pe = PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);
		AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		if (Build.VERSION.SDK_INT >= 21) {
			alarm.setAlarmClock(new AlarmManager.AlarmClockInfo(time, pe), pe);
		} else if (Build.VERSION.SDK_INT >= 19) {
			alarm.setExact(AlarmManager.RTC_WAKEUP, time, pe);
		} else {
			alarm.set(AlarmManager.RTC_WAKEUP, time, pe);
		}
	}


}
