package com.bestbizsvc.zzzpal;


import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;

public class SettingsActivity
extends AppCompatPreferenceActivity
{

	private static Preference.OnPreferenceChangeListener
			sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
		@Override
		public boolean onPreferenceChange (Preference preference, Object value) {
			String stringValue = value.toString();
			preference.setSummary(stringValue);

			return true;
		}
	};

	private static void bindPreferenceSummaryToValue (Preference preference) {
		preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

		sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
				PreferenceManager
						.getDefaultSharedPreferences(preference.getContext())
						.getString(preference.getKey(), ""));
	}

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setupActionBar();
		// Display the fragment as the main content.
		getFragmentManager().beginTransaction() // i.e. I don't want separate headers pointing to several pages.
				.replace(android.R.id.content, new GeneralPreferenceFragment())
				.commit(); // instead, one page with all the settings is OK
	}



	private void setupActionBar () {
		ActionBar actionBar = getSupportActionBar();
		if(actionBar != null) {
			// Show the Up button in the action bar.
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setDisplayShowHomeEnabled(true);
		}
	}

	@Override
	public boolean onIsMultiPane () {
		return false;
	}



	protected boolean isValidFragment (String fragmentName) {
		return PreferenceFragment.class.getName().equals(fragmentName)
				;
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public static class GeneralPreferenceFragment extends PreferenceFragment {
		@Override
		public void onCreate (Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			addPreferencesFromResource(R.xml.pref_general);
			bindPreferenceSummaryToValue(findPreference("pulse_duration"));
			bindPreferenceSummaryToValue(findPreference("pulse_number"));
			bindPreferenceSummaryToValue(findPreference("main_cycle_minutes"));
			bindPreferenceSummaryToValue(findPreference("alarm_cycle_minutes"));
			setHasOptionsMenu(true);
		}

		@Override
		public boolean onOptionsItemSelected (MenuItem item) {
			int id = item.getItemId();
			if(id == android.R.id.home) {
				NavUtils.navigateUpFromSameTask(this.getActivity());
				return true;
			}
			return super.onOptionsItemSelected(item);
		}
	}
}
