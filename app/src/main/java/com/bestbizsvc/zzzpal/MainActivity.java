package com.bestbizsvc.zzzpal;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.SimpleDateFormat;

import static com.bestbizsvc.zzzpal.AlarmService.ACTION_MAIN;
import static com.bestbizsvc.zzzpal.AlarmService.ACTION_TEST;


public class MainActivity
extends AppCompatActivity
{
	TextView m_tv;
	String m_privateFileName = "zzzFile.txt";


	private void showAndLog (String s) {
		FileOutputStream outputStream;

		SimpleDateFormat dateformat = new SimpleDateFormat("hh:mm:ss");
		long nowMillis=System.currentTimeMillis();
		String nowTime = dateformat.format(nowMillis);
		s=nowTime+": "+s+"\n";

		m_tv.append(s);

		try {
			Context ctx = getApplicationContext();
			File privFile = new File(ctx.getFilesDir(), m_privateFileName);
			FileWriter fw = new FileWriter(privFile.getAbsoluteFile(), true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(s);
			if (bw != null)
				bw.close();

			if (fw != null)
				fw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		m_tv = (TextView) findViewById(R.id.tvStatus);
		m_tv.setMovementMethod(new ScrollingMovementMethod());
		m_tv.setText("");

		Context ctx = getApplicationContext();
		File privFile = new File(ctx.getFilesDir(), m_privateFileName);
		try{
			if(privFile.exists()) {
				FileReader fr = new FileReader(privFile.getAbsoluteFile());
				BufferedReader br = new BufferedReader(fr);
				String s;
				while((s = br.readLine()) != null) {
					m_tv.append(s+"\n");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}


		m_tv.setOnLongClickListener(new View.OnLongClickListener() {
			@Override
			public boolean onLongClick (View v) {
				((TextView) v).setText("");
				Context ctx = getApplicationContext();
				File privFile = new File(ctx.getFilesDir(), m_privateFileName);
				privFile.delete();
				return false;
			}
		});
	}

	public void performTest (View view) {
		Intent intent = new Intent(MainActivity.this, AlarmService.class);

		intent.setAction(ACTION_TEST);

		showAndLog("performing test");

		Handler handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				Bundle reply = msg.getData();
				String strR=(String)reply.getSerializable("msg");
				showAndLog(strR);
			}
		};

		intent.putExtra("messenger", new Messenger(handler));
		startService(intent);
	}


	public void startMain (View view) {
		Intent intent = new Intent(MainActivity.this, AlarmService.class);
		intent.setAction(ACTION_MAIN);

		Handler handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				Bundle reply = msg.getData();
				String strR=(String)reply.getSerializable("msg");
				showAndLog(strR);
			}
		};

		intent.putExtra("messenger", new Messenger(handler));

		showAndLog("Starting main cycle");

		startService(intent);
	}

	public void onSettings (View view) {// vedi classe SettingsActivity
		Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
		startActivity(intent);
	}


	public void onExit (View view) {
		finish();
		System.exit(0);
	}
}

